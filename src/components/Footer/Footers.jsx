import React from "react";
import styleFooter from "./styleFooter.module.css";
import IconFacebook from './IconFacebook.png';
import IconInstagram from './IconInstagram.png';
import IconTwitter from './IconTwitter.png';
import fi_logo from './fi_logo.png';

function footers() {
    return (
        <div className={styleFooter.styleFooter}>
            <div className="container footer my-2 d-flex justify-content-between flex-column flex-md-row gap-5">
                <div className="d-flex flex-column">
                    <p className={styleFooter.styleTextAddress}>
                        Jalan Suroyo No. 161 Mayangan Kota
                        <br />
                        Probolonggo 672000
                    </p>
                    <p className={styleFooter.styleTextAddress}>tokoku@gmail.com</p>
                    <p className={styleFooter.styleTextAddress}>081-233-334-808</p>
                </div>
                <div className="d-flex flex-column">
                    <a href="#login" className={styleFooter.styleTextLink}>
                        Login
                    </a>
                    <a href="#whyus" className={styleFooter.styleTextLink}>
                        Daftar
                    </a>
                    <a
                        href="#testimonial"
                        className={styleFooter.styleTextLink}>

                        Produk
                    </a>
                    <a href="#faq" className={styleFooter.styleTextLink}>
                        FAQ
                    </a>
                </div>
                <div className="d-flex flex-column">
                    <p className={styleFooter.styleFooterText}>Connect with us</p>
                    <div className="flex">
                        <img className={styleFooter.styleIcon} alt="ig" src={IconInstagram} />
                        <img className={styleFooter.styleIcon} alt="footer1" src={IconFacebook} />
                        <img className={styleFooter.styleIcon} alt="footer3" src={IconTwitter} />
                    </div>
                </div>
                <div>
                    <p className={styleFooter.styleFooterText}>© Copyright Tokoku 2022</p>
                    <img alt="logo" className={styleFooter.styleLogo} src={fi_logo} />
                </div>
            </div>
        </div>
    );
}

export default footers;
