import Carousel from 'react-bootstrap/Carousel';
import React from 'react';
import banner1 from './banner1.jpg';
import styleCarousels from './styleCarousels.module.css';


function Carousels() {
    return (
        <Carousel className={styleCarousels.styleCarousel}>
            <Carousel.Item className={styleCarousels.styleItem}>
                <img
                    className={styleCarousels.styleImg}
                    src={banner1}
                    alt="First slide"
                />
            </Carousel.Item>
            <Carousel.Item className={styleCarousels.styleItem}>
                <img
                    className={styleCarousels.styleImg}
                    src={banner1}
                    alt="First slide"
                />
            </Carousel.Item>
            <Carousel.Item className={styleCarousels.styleItem}>
                <img
                    className={styleCarousels.styleImg}
                    src={banner1}
                    alt="First slide"
                />
            </Carousel.Item>
        </Carousel>
    );
}

export default Carousels;