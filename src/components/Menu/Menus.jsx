import React from "react";
import { Button } from "react-bootstrap";
import styleMenus from './styleMenus.module.css';
import fi_search from './fi_search.png'


function Menus() {
    return (
        <div className={styleMenus.styleContainer}>
            <Button className={styleMenus.styleButton}>
                <img className={styleMenus.imgIcon} src={fi_search} alt="" type="search" />
                <p className={styleMenus.styleText}>Semua</p>
            </Button>
            <Button className={styleMenus.styleButton}>
                <img className={styleMenus.imgIcon} src={fi_search} alt="" type="search" />
                <p className={styleMenus.styleText}>Hobi</p>
            </Button>
            <Button className={styleMenus.styleButton}>
                <img className={styleMenus.imgIcon} src={fi_search} alt="" type="search" />
                <p className={styleMenus.styleText}>Kendaraan</p>
            </Button>
            <Button className={styleMenus.styleButton}>
                <img className={styleMenus.imgIcon} src={fi_search} alt="" type="search" />
                <p className={styleMenus.styleText}>Baju</p>
            </Button>
            <Button className={styleMenus.styleButton}>
                <img className={styleMenus.imgIcon} src={fi_search} alt="" type="search" />
                <p className={styleMenus.styleText}>Elektronik</p>
            </Button>
            <Button className={styleMenus.styleButton}>
                <img className={styleMenus.imgIcon} src={fi_search} alt="" type="search" />
                <p className={styleMenus.styleText}>Kesehatan</p>
            </Button>
        </div>
    )
}

export default Menus