import React from 'react';
import styleNavbars from './styleNavbars.module.css';
import { Row, Navbar, Nav, Form, Container, Button } from 'react-bootstrap';
import fi_logo from "./fi_logo.png"
import fi_login from "./fi_login.png"
import fi_search from "./fi_search.png"


function Navbars() {
    return (
        <Navbar expand="lg" className={styleNavbars.styleNavbar}>
            <Container>
                <Row className={styleNavbars.brands}>
                    <Nav>
                        <Nav.Link href="#">
                            <img src={fi_logo} className={styleNavbars.styleLogo} alt="halo" />
                        </Nav.Link>
                    </Nav>
                </Row>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav" className='justify-content-between'>
                    <Row>
                        <Nav>
                            <Form className={styleNavbars.search}>
                                <Form.Control
                                    type="search here"
                                    placeholder="Cari disini"
                                    aria-label="Search"
                                    className={styleNavbars.styleFormSearch}

                                />
                                <img src={fi_search} alt="search" className={styleNavbars.styleIconSearch} />
                            </Form>
                        </Nav>
                    </Row>

                    <Row>
                        <a href="/login">
                            <Button className={styleNavbars.styleLogin}>
                                <img src={fi_login} alt="" />
                                <p className={styleNavbars.styleText}>Login</p>
                            </Button>

                        </a>
                    </Row>
                </Navbar.Collapse>

            </Container>
        </Navbar>
    );
}

export default Navbars;