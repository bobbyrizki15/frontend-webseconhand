import { Card, Row, Col } from "react-bootstrap";
import styleCards from "./styleCards.module.css";
import img1 from "./img1.png";
import img2 from "./img2.png";

function Cards() {
  return (
    <Row className={styleCards.styleRow}>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img1} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img2} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img1} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img2} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img1} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img2} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img1} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img2} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img1} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
      <Col className={styleCards.StyleCol}>
        <Card className={styleCards.productCard}>
          <Card.Img className={styleCards.imgCard} variant="top" src={img2} />
          <Card.Body className="px-1 py-0">
            <h5 className={styleCards.titleText}>Jam Tangan Casio</h5>
            <p className={styleCards.categoryText}>Aksesoris</p>
            <h5 className={styleCards.titleText}>Rp. 250.000</h5>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}

export default Cards;