import Navbars from '../../components/Navbar/Navbars';
import Carousels from '../../components/Carousel/Carousels';
import Cards from '../../components/Card/Cards';
import Menus from '../../components/Menu/Menus';
import Footers from '../../components/Footer/Footers';

import React from "react";


function Home() {
  return (
    <div>
      <Navbars />
      <Carousels />
      <Menus />
      <Cards />
      <Footers />

    </div>
  );
}

export default Home;
